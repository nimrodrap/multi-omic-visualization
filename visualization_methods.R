log.transform.data <- function(omics.list, dataset.info) {
  return(omics.list) # TODO: change
  omic.info = dataset.info$omic.type
  omics.list = lapply(1:length(omics.list), function(i) {
    if (omic.info[i] == 'RNAseq') {
      return(log(1 + omic.list[i]))
    } else {
      return(omic.list[i])
    }
  })
  return(omics.list)
}

filter.and.normalize <- function(omics.data, normalize=T, filter.var=F) {

  if (filter.var) {
    omics.data = lapply(omics.data, keep.high.var.features)
  }
  
  if (normalize) {
    omics.data = lapply(omics.data, normalize.matrix)    
  }
  
  return(omics.data)
}

keep.high.var.features <- function(omic, num.features=2000) {
  if (nrow(omic) < num.features) {
    return(omic)
  } else {
    feature.vars = apply(omic, 1, var)
    threshold = feature.vars[order(feature.vars, decreasing = T)][num.features]
    return(omic[feature.vars >= threshold,])    
  }
}

normalize.matrix <- function(data.matrix) {
  temp = data.matrix - rowMeans(data.matrix)
  should.keep = (apply(temp, 1, sd) != 0)
  return ((temp / apply(temp, 1, sd))[should.keep, ])
}

.do.pca <- function(omic, dimension) {
  omic = omic - rowMeans(omic)
  omic = omic / apply(omic, 1, sd)
  #repr = t(prcomp(t(concat.omics))$x[,1:dimension])
  
  svd.decom = svd(t(omic), dimension)
  repr = t(svd.decom$u) * svd.decom$d[1:dimension]
  return(repr)
}

run.pca <- function(omics.list, dataset.info, dimension) {
  # TODO: Decide on the normalization for each dataset.
  start = Sys.time()
  omics.list = log.transform.data(omics.list)
  concat.omics = do.call(rbind, omics.list)
  repr = .do.pca(concat.omics, dimension)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.tsne <- function(omics.list, dataset.info, dimension) {
  #TODO: so I do pca and then concat, or concat and then pca? if the first, should I normalize the PCs somehow?
  #TODO: should change tsne implementation...
  start = Sys.time()
  
  #omics.list = log.transform.data(omics.list, dataset.info)
  #omics.list = lapply(omics.list, function(omic) .do.pca(omic, 25))


  concat.omics = do.call(rbind, omics.list)
  pca.dim = min(nrow(concat.omics), 100)
  concat.omics = .do.pca(concat.omics, pca.dim)


  # initial_dims is the number of dimensions used in whitening, which is not used since whiten=F.
  # We should decide what to do here with the whitening.
  #repr = t(tsne(X=t(concat.omics), k=dimension, initial_dims=nrow(concat.omics), whiten=F))
  #repr = t(tsne(X=t(concat.omics), k=dimension))
  #repr = t(Rtsne(X=t(concat.omics), dims=dimension)$Y)
  repr = t(Rtsne(X=t(concat.omics), dims=dimension, whiten=F, check_duplicates=F)$Y)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.umap <- function(omics.list, dataset.info, dimension, max.pca.dim=100) {
  #TODO: same issues as in t-sne.
  start = Sys.time()
  omics.list = log.transform.data(omics.list)
  #omics.list = lapply(omics.list, function(omic) .do.pca(omic, 100))

  concat.omics = do.call(rbind, omics.list)
  pca.dim = min(nrow(concat.omics), max.pca.dim)
  concat.omics = .do.pca(concat.omics, pca.dim)

  umap.conf = umap.defaults
  umap.conf$n_components = dimension
  # parameter selection is based on recommended values from the github page. TODO: or was it 10 neighbors?
  umap.ret = umap(t(concat.omics), umap.conf, min.dist=0.1, n_neighbors=15, metric='euclidean', random_state=42)
  repr = t(umap.ret$layout)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.mcca <- function(omics.list, dataset.info, dimension, rep.omic=1) {
  stopifnot(length(omics.list) > 1)
  start = Sys.time()
  omics.list = log.transform.data(omics.list)
  omics.list = filter.and.normalize(omics.list, normalize = T, filter.var = T)
  
  subtype = dataset.info$name
  omics.transposed = lapply(omics.list, t)
  
  # Code for penalty is taken from MultiCCA.permute method
  #penalty <- lapply(omics.transposed, function(omic) pmax(seq(.1, .8, len=10)*sqrt(ncol(omic)),1.1))
  cca.ret = PMA::MultiCCA(omics.transposed, 
                          ncomponents = dimension, penalty=1)
  #cca.ret = PMA::MultiCCA(omics.transposed, ncomponents = dimension)
  sample.rep = t(omics.transposed[[rep.omic]] %*% cca.ret$ws[[rep.omic]])
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=sample.rep, timing=time.taken))
}


run.swne <- function(omics.list, dataset.info, dimension) {
  #TODO: make sure that > 2 dimension is not supported
  stopifnot(dimension==2)
  start = Sys.time()
  concat.omics = do.call(rbind, omics.list)
  # TODO: set k.range instead of k
  swne.ret = RunSWNE(as.matrix(concat.omics), k=10, genes.embed=c(), var.genes=rownames(concat.omics))
  repr = t(swne.ret$sample.coords)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.msne <- function(omics.list, dataset.info, dimension) {
  start = Sys.time()
  omics.list = log.transform.data(omics.list)
  #omics.list = lapply(omics.list, function(omic) .do.pca(omic, 100))
  omics.list = lapply(omics.list, function(omic) .do.pca(omic, min(100, nrow(omic))))

  #concat.omics = do.call(rbind, omics.list)
  #pca.dim = min(nrow(concat.omics), 100)
  #concat.omics = .do.pca(concat.omics, pca.dim)

  #msne.ret = mv.tsne(X=list(concat.omics), low.dim=dimension)
  msne.ret = mv.tsne(X=omics.list, low.dim=dimension)
  repr = t(msne.ret[[1]])
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.mofa <- function(omics.list, dataset.info, dimension) {
  #TODO
  start = Sys.time()
  mofa.obj <- createMOFAobject(omics.list)
  data.options <- getDefaultDataOptions()
  model.options <- getDefaultModelOptions(mofa.obj)
  model.options$numFactors = dimension
  train.options <- getDefaultTrainOptions()
  train.options$seed <- 42

  mofa.obj <- prepareMOFA(mofa.obj, DataOptions = data.options, 
                    ModelOptions = model.options, TrainOptions = train.options)
  # The next call may fail if mofa does not find the correct python.
  mofa.obj <- runMOFA(mofa.obj)
  repr = t(getFactors(mofa.obj), as.data.frame=T)

  # The next line may happen since the model may decide to drop a factor.
  stopifnot(nrow(repr) == dimension)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken))
}


run.mswne <- function(omics.list, dataset.info, dimension) {
  stopifnot(dimension==2)
  
  # TODO: should maybe take the precalculated result?
  #multinmf.ret = run.nmf(omics.list, dataset.info, dimension)
  
  tmp.pca = run.pca(omics.list, dataset.info, 10)
  nmf.time = tmp.pca$timing
  nmf.repr = tmp.pca$repr - min(tmp.pca$repr)

  #nmf.res.path = file.path(CONFIG$results.dir, 'nmf', dataset.info$name)
  #load(nmf.res.path)
  #nmf.time = method.ret$timing
  #nmf.repr = method.ret$repr

  start = Sys.time()
  nemo.sim = nemo.affinity.graph(omics.list) # TODO: should I set the diag to 1? some other normalization?
  # SNN computation is currently like this: create 1-0 matrix with knn of each sample (not symmetric). Denote SNN.
  # calculate SNN * t(SNN). (number of common k nearest neighbors). 
  # change each value x to x / (2k - x), and prune values < some threshold (1/15 by default). Set diag to 1.
  swne.ret = EmbedSWNE(nmf.repr, Matrix(nemo.sim), n_pull=3)
  repr = t(swne.ret$sample.coords)
  time.taken = as.numeric(Sys.time() - start, units='secs')
  return(list(repr=repr, timing=time.taken + nmf.time))
}


run.nmf <- function(omics.list, dataset.info, dimension) {

  # clear nmf directory
  multinmf.dir = CONFIG$multinmf.dir.path
  file.names = list.files(multinmf.dir)
  file.remove(file.path(multinmf.dir, file.names))
  solution.path = file.path(multinmf.dir, 'solution')
  timing.path = file.path(multinmf.dir, 'timing')
  

  total.time.taken = 0
  start = Sys.time()

  omics.list = log.transform.data(omics.list, dataset.info)
  omics.list = filter.and.normalize(omics.list, filter.var = T, normalize = F)

  time.taken = as.numeric(Sys.time() - start, units='secs')
  total.time.taken = total.time.taken + time.taken
  
  .unused = lapply(1:length(omics.list), function(i) write.table(omics.list[[i]], file=file.path(multinmf.dir, i)))

  if (length(omics.list) > 1) {
    command.to.run = sprintf('%s -nodisplay -wait -nosplash -nodesktop -r "num_omics=%d;dimension=%d;run(\'%s\');exit;"', 
      CONFIG$matlab.path, length(omics.list), dimension, CONFIG$multinmf.script.path)
    command.ret = system(command.to.run)
    stopifnot(command.ret == 0)
    nmf.timing = read.csv(timing.path, header=F)[1, 1]
    total.time.taken = total.time.taken + nmf.timing
  } else {
    start = Sys.time()
    file.name = paste0(solution.path)
    nmf.ret = nmf(omics.list[[1]], dimension, method='lee')
    coef.mat = t(coef(nmf.ret))
    time.taken = as.numeric(Sys.time() - start, units='secs')
    total.time.taken = total.time.taken + time.taken
    write.table(coef.mat, file=file.name, quote=F, row.names=F, col.names=F, sep=',')
  }
  
  repr = read.csv(solution.path, header=F)
  return(list(repr=repr, timing=total.time.taken))  
}

